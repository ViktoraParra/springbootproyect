package ar.com.victorparra.emailsender.SmsMessageApp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("twilio")
public class TwilioConfg {

    private String accountSid;

    private String authToken;

    private String trialNumnber;

    public TwilioConfg() {
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getTrialNumnber() {
        return trialNumnber;
    }

    public void setTrialNumnber(String trialNumnber) {
        this.trialNumnber = trialNumnber;
    }
}
