package ar.com.victorparra.emailsender.SmsMessageApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsMessageAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsMessageAppApplication.class, args);
	}

}
