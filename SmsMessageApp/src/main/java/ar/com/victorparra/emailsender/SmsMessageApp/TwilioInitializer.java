package ar.com.victorparra.emailsender.SmsMessageApp;

import com.twilio.Twilio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TwilioInitializer {

    private final static Logger LOGGER = LoggerFactory.getLogger(TwilioInitializer.class);

    private final TwilioConfg twilioConfg;

    @Autowired
    public TwilioInitializer(TwilioConfg twilioConfg) {
        this.twilioConfg = twilioConfg;
        Twilio.init(
                twilioConfg.getAccountSid(),
                twilioConfg.getAuthToken()
        );
        LOGGER.info("Twilio initialized ... with account sid {} ", twilioConfg.getAccountSid());
    }
}
