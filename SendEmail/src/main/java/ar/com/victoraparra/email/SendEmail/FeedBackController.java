package ar.com.victoraparra.email.SendEmail;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ValidationException;

@RestController
@RequestMapping("/feedback")
public class FeedBackController {

    private EmailConfiguration emailCfg;

    public FeedBackController(EmailConfiguration emailCfg) {
        this.emailCfg = emailCfg;
    }

    @PostMapping
    public void sendFeedBack(@RequestBody FeedBack feedBack, BindingResult bindingResult) {

        if (bindingResult.hasErrors()){
            throw new ValidationException("Feedback is not valid");
        }

        // Crear enviador de correo
        // realizamos la conficuracion conectando con la configuracion de nuestro
        // encargado de enviar los mensaje determinando Host, Port, Username y Password
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(this.emailCfg.getHost());
        mailSender.setPort(this.emailCfg.getPort());
        mailSender.setUsername(this.emailCfg.getUsername());
        mailSender.setPassword(this.emailCfg.getPassword());

        // Creando la instancia del correo
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(feedBack.getEmail());
        mailMessage.setTo("victoraparrapineda@gmail.com");
        mailMessage.setSubject("respuesta automatica de " + feedBack.getName());
        mailMessage.setText(feedBack.getFeedback());

        // Enviamos el correo con encargado de hacer el envio
        mailSender.send(mailMessage);
    }
}
